import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import json

# Change this path
filename = r"/mnt/c/Users/Suelen/Downloads/MICRODADOS_ENEM_2019.csv"

pd.options.display.max_columns = None
df = pd.read_csv(filename, sep=";", encoding="ISO-8859-1", chunksize=1000000)
df = pd.concat(df, ignore_index=True)
df.head()

# Picking up only columns that will be used
small_df = df[["SG_UF_PROVA", "NU_NOTA_CN", "NU_NOTA_CH", "NU_NOTA_LC", "NU_NOTA_MT", "NU_NOTA_REDACAO"]]

# Removing NaN values
# Reference: https://towardsdatascience.com/whats-the-best-way-to-handle-nan-values-62d50f738fc
small_df = small_df.dropna()
# To reset the indices
small_df = small_df.reset_index(drop = True)

# Calculating the media and inserting it in a new column NOTA_MEDIA
small_df["NOTA_MEDIA"] = (small_df["NU_NOTA_CN"] + small_df["NU_NOTA_CH"] +
                          small_df["NU_NOTA_LC"] + small_df["NU_NOTA_MT"] +
                          small_df["NU_NOTA_REDACAO"]) / 5
small_df["NOTA_MEDIA"] = small_df["NOTA_MEDIA"].round(2)

# save dataframe in CSV file to be used in D3 framework
# small_df["NOTA_MEDIA"].to_csv("ENEM_processed.csv")

# Creates new dataframes according to the region
dic_regions = {"Northeast": ["AL", "PE", "PB", "SE", "BA", "RN", "PI", "MA", "CE"], 
               "North": ["AC", "AM", "AP", "PA", "TO", "RR", "RO"],
               "Midwest": ["DF", "MT", "MS", "GO"],
               "Southeast": ["RJ", "SP", "ES", "MG"], 
               "South": ["PR", "RS", "SC"]}

df_N = small_df.loc[(small_df["SG_UF_PROVA"].values == "AC") | (small_df["SG_UF_PROVA"].values == "AM") | 
                    (small_df["SG_UF_PROVA"].values == "AP") | (small_df["SG_UF_PROVA"].values == "PA") |
                    (small_df["SG_UF_PROVA"].values == "TO") | (small_df["SG_UF_PROVA"].values == "RR") |
                    (small_df["SG_UF_PROVA"].values == "RO")]
df_N = df_N.drop(df_N.columns[[1,2,3,4,5]], axis=1)

df_NE = small_df.loc[(small_df["SG_UF_PROVA"].values == "AL") | (small_df["SG_UF_PROVA"].values == "PE") | 
                     (small_df["SG_UF_PROVA"].values == "PB") | (small_df["SG_UF_PROVA"].values == "SE") |
                     (small_df["SG_UF_PROVA"].values == "BA") | (small_df["SG_UF_PROVA"].values == "RN") |
                     (small_df["SG_UF_PROVA"].values == "PI") | (small_df["SG_UF_PROVA"].values == "MA") |
                     (small_df["SG_UF_PROVA"].values == "CE")]
df_NE = df_NE.drop(df_NE.columns[[1,2,3,4,5]], axis=1)

df_CO = small_df.loc[(small_df["SG_UF_PROVA"].values == "DF") | (small_df["SG_UF_PROVA"].values == "MT") | 
                     (small_df["SG_UF_PROVA"].values == "MS") | (small_df["SG_UF_PROVA"].values == "GO")]
df_CO = df_CO.drop(df_CO.columns[[1,2,3,4,5]], axis=1)

df_SE = small_df.loc[(small_df["SG_UF_PROVA"].values == "RJ") | (small_df["SG_UF_PROVA"].values == "SP") | 
                     (small_df["SG_UF_PROVA"].values == "ES") | (small_df["SG_UF_PROVA"].values == "MG")]
df_SE = df_SE.drop(df_SE.columns[[1,2,3,4,5]], axis=1)

df_S = small_df.loc[(small_df["SG_UF_PROVA"].values == "PR") | (small_df["SG_UF_PROVA"].values == "RS") | 
                    (small_df["SG_UF_PROVA"].values == "SC")]
df_S = df_S.drop(df_S.columns[[1,2,3,4,5]], axis=1)

# Creates the JSON file to be used for visualization in D3
# We categorize the candidates' grade per region in Brazil
def bins_set(df):
    bins_dict = {}

    for i in range(5, 55, 5):
        hist_bin = np.histogram_bin_edges(df["NOTA_MEDIA"], bins=i, range=(0, 1000))
        results = df["NOTA_MEDIA"].value_counts(bins=hist_bin, sort=False)

        bins_dict[i] = results.tolist()

    return bins_dict


regions = list(dic_regions.keys())
df_regions = {"Northeast": df_NE, "North": df_N, "Midwest": df_CO, "Southeast": df_SE, "South": df_S}
def average_per_region():
    region_data = {}

    for region in regions:
        if region in list(df_regions.keys()):
            region_data[region] = bins_set(df_regions[region])
        
    return region_data


try:
    data = average_per_region()
    print("===========================================================")
    if len(data) >= 0:
        with open('ENEM_preprocessed.json', 'w') as outfile:
            json.dump(data, outfile)
            print(data)

except Exception as e:
    print(e)
    print("Error!")
print("###################### Successfully! Created JSON file. ##############################")

# Histogram as example of data reduction technique and visualization (data representation)

## Bin - considering the entire Brazil data (candidates' average)
bin_edges_BR = np.histogram_bin_edges(small_df["NOTA_MEDIA"], bins=10, range=(0, 1000))

## Bin - considering only Southeast region data (candidates' average)
bin_edges_SE = np.histogram_bin_edges(df_SE["NOTA_MEDIA"], bins=10, range=(0, 1000))

plt.figure(figsize=(8,6))
plt.hist(small_df["NOTA_MEDIA"], bins=bin_edges_BR, log=True, edgecolor="black", alpha=0.5, label="Brazil")
plt.hist(df_SE["NOTA_MEDIA"], bins=bin_edges_SE, log=True, edgecolor="black", alpha=0.5, label="Southeast Region")
plt.title("ENEM 2019 - Histogram with binning\n", fontsize=16, fontweight="bold")

plt.xticks(bin_edges_BR, fontsize=12)
plt.xticks(bin_edges_SE, fontsize=12)
plt.yticks(fontsize=12)

plt.xlabel("\nRange of candidate averages", fontsize=14)
plt.ylabel("Number of candidates (in log)\n", fontsize=14)
plt.legend(loc='upper right')
# plt.savefig("histogram_ENEM_2019.png")
plt.show()
