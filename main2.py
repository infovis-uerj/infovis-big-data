import numpy as np
import pandas as pd
import json

# Change this path
filename = "C:/Users/rycba/OneDrive/Documentos/dados_enem/MICRODADOS_ENEM_2019.csv"

pd.options.display.max_columns = None
df = pd.read_csv(filename, sep=";", encoding="ISO-8859-1", chunksize=1000000)
df = pd.concat(df, ignore_index=True)
df.head()

# Picking up only columns that will be used

small_df = df[["SG_UF_PROVA", "TP_SEXO", "TP_COR_RACA", "NU_IDADE", "TP_ESCOLA"]]

# Removing NaN values
# Reference: https://towardsdatascience.com/whats-the-best-way-to-handle-nan-values-62d50f738fc
small_df = small_df.dropna()
# To reset the indices
small_df = small_df.reset_index(drop=True)


# Creates new dataframes according to the region
dic_regions = {"Northeast": ["AL", "PE", "PB", "SE", "BA", "RN", "PI", "MA", "CE"],
               "North": ["AC", "AM", "AP", "PA", "TO", "RR", "RO"],
               "Midwest": ["DF", "MT", "MS", "GO"],
               "Southeast": ["RJ", "SP", "ES", "MG"],
               "South": ["PR", "RS", "SC"]}



df_N = small_df.loc[(small_df["SG_UF_PROVA"].values == "AC") | (small_df["SG_UF_PROVA"].values == "AM") |
                    (small_df["SG_UF_PROVA"].values == "AP") | (small_df["SG_UF_PROVA"].values == "PA") |
                    (small_df["SG_UF_PROVA"].values == "TO") | (small_df["SG_UF_PROVA"].values == "RR") |
                    (small_df["SG_UF_PROVA"].values == "RO")]


df_NE = small_df.loc[(small_df["SG_UF_PROVA"].values == "AL") | (small_df["SG_UF_PROVA"].values == "PE") |
                     (small_df["SG_UF_PROVA"].values == "PB") | (small_df["SG_UF_PROVA"].values == "SE") |
                     (small_df["SG_UF_PROVA"].values == "BA") | (small_df["SG_UF_PROVA"].values == "RN") |
                     (small_df["SG_UF_PROVA"].values == "PI") | (small_df["SG_UF_PROVA"].values == "MA") |
                     (small_df["SG_UF_PROVA"].values == "CE")]


df_CO = small_df.loc[(small_df["SG_UF_PROVA"].values == "DF") | (small_df["SG_UF_PROVA"].values == "MT") |
                     (small_df["SG_UF_PROVA"].values == "MS") | (small_df["SG_UF_PROVA"].values == "GO")]


df_SE = small_df.loc[(small_df["SG_UF_PROVA"].values == "RJ") | (small_df["SG_UF_PROVA"].values == "SP") | (small_df["SG_UF_PROVA"].values == "ES") | (small_df["SG_UF_PROVA"].values == "MG")]


df_S = small_df.loc[(small_df["SG_UF_PROVA"].values == "PR") | (small_df["SG_UF_PROVA"].values == "RS") | (small_df["SG_UF_PROVA"].values == "SC")]



# Creates the JSON file to be used for visualization in D3
# We categorize the candidates' per region in Brazil
def extract_sexo(dfsex):
    jsonSexo = dfsex["TP_SEXO"].value_counts().to_json(orient="index")
    jsonSexo = json.loads(jsonSexo)


    sexo = {"valores" : {
        "M" : jsonSexo["M"],
        "F" : jsonSexo["F"]
    }, "porcentagem" : {
        "M": float(float(jsonSexo["M"])/float((float(jsonSexo["F"]) + float(jsonSexo["M"])))),
        "F": float(float(jsonSexo["F"])/float((float(jsonSexo["F"]) + float(jsonSexo["M"]))))
    }}
    return sexo

def extract_idade(dfid):
    jsonIdade = dfid["NU_IDADE"].value_counts().to_json(orient="index")
    jsonIdade = json.loads(jsonIdade)
    idoso = 0
    jovem = 0
    adulto = 0
    for idade in jsonIdade:
        if int(idade) < 20:
            jovem += jsonIdade[idade]
        elif int(idade) < 60:
            adulto += jsonIdade[idade]
        else:
            idoso += jsonIdade[idade]
    idade = { "valores" : {
        "jovem": jovem,
        "adulto": adulto,
        "idoso": idoso
         }, "porcentagem" : {
        "jovem": float(float(jovem)/(float(jovem) + float(adulto) + float(idoso))),
        "adulto": float(float(adulto)/(float(jovem) + float(adulto) + float(idoso))),
        "idoso": float(float(idoso)/(float(jovem) + float(adulto) + float(idoso)))
    }
              }
    return idade

def extract_raca(dfrac):
    jsonRaca = dfrac["TP_COR_RACA"].value_counts().to_json(orient="index")
    jsonRaca = json.loads(jsonRaca)

    raca = { "valores" : {
        "naoDeclarado" : jsonRaca["0"],
        "branca" : jsonRaca["1"],
        "preta" : jsonRaca["2"],
        "parda" : jsonRaca["3"],
        "amarela" : jsonRaca["4"],
        "indigena" : jsonRaca["5"]
    },
        "porcentagem" : {
            "naoDeclarado": float(float(jsonRaca["0"])/(float(jsonRaca["0"])+float(jsonRaca["1"])+float(jsonRaca["2"])+float(jsonRaca["3"])+float(jsonRaca["4"])+float(jsonRaca["5"]))),
            "branca": float(float(jsonRaca["1"])/(float(jsonRaca["0"])+float(jsonRaca["1"])+float(jsonRaca["2"])+float(jsonRaca["3"])+float(jsonRaca["4"])+float(jsonRaca["5"]))),
            "preta": float(float(jsonRaca["2"])/(float(jsonRaca["0"])+float(jsonRaca["1"])+float(jsonRaca["2"])+float(jsonRaca["3"])+float(jsonRaca["4"])+float(jsonRaca["5"]))),
            "parda": float(float(jsonRaca["3"])/(float(jsonRaca["0"])+float(jsonRaca["1"])+float(jsonRaca["2"])+float(jsonRaca["3"])+float(jsonRaca["4"])+float(jsonRaca["5"]))),
            "amarela": float(float(jsonRaca["4"])/(float(jsonRaca["0"])+float(jsonRaca["1"])+float(jsonRaca["2"])+float(jsonRaca["3"])+float(jsonRaca["4"])+float(jsonRaca["5"]))),
            "indigena": float(float(jsonRaca["5"])/(float(jsonRaca["0"])+float(jsonRaca["1"])+float(jsonRaca["2"])+float(jsonRaca["3"])+float(jsonRaca["4"])+float(jsonRaca["5"])))
        }
    }
    return raca

def extract_escola(dfesc):
    jsonEscola = dfesc["TP_ESCOLA"].value_counts().to_json(orient="index")
    jsonEscola = json.loads(jsonEscola)

    escola = { "valores" : {
        "naoRespondeu" : jsonEscola["1"],
        "publica" : jsonEscola["2"],
        "privada" : jsonEscola["3"]
    },
        "porcentagem" : {
            "naoRespondeu": float(float(jsonEscola["1"])/ (float(jsonEscola["1"]) + float(jsonEscola["2"]) + float(jsonEscola["3"]))),
            "publica": float(float(jsonEscola["2"])/ (float(jsonEscola["1"]) + float(jsonEscola["2"]) + float(jsonEscola["3"]))),
            "privada": float(float(jsonEscola["3"])/ (float(jsonEscola["1"]) + float(jsonEscola["2"]) + float(jsonEscola["3"])))
        }

    }
    return escola

def info_set_region(region):
    df_reg = small_df.loc[(small_df["SG_UF_PROVA"].values == region)]
    json_idade = extract_idade(df_reg)
    json_sexo = extract_sexo(df_reg)
    json_raca = extract_raca(df_reg)
    json_escola = extract_escola(df_reg)

    json = {"sexo" : json_sexo,
            "idade" : json_idade,
            "raca" : json_raca,
            "escolaridade" : json_escola}


    return json

def info_set_regionOut(df_reg_out):
    json_idade = extract_idade(df_reg_out)
    json_sexo = extract_sexo(df_reg_out)
    json_raca = extract_raca(df_reg_out)
    json_escola = extract_escola(df_reg_out)

    json = {"sexo" : json_sexo,
            "idade" : json_idade,
            "raca" : json_raca,
            "escolaridade" : json_escola}


    return json
regions = list(dic_regions.keys())

df_regions = {"Northeast": df_NE, "North": df_N, "Midwest": df_CO, "Southeast": df_SE, "South": df_S}

def average_per_region():
    region_data = {}
    region_inner = {}

    x = 0
    for region in regions:
        region_inner = {}
        region_out = {}
        for reg in dic_regions[region]:
            region_inner[reg] = info_set_region(reg)
        region_out = {"total" : info_set_regionOut(df_regions[region]), "individual" : region_inner }
        region_data[region] = region_out
    return region_data



try:
    data = average_per_region()
    print("===========================================================")
    if len(data) >= 0:
        with open('ENEM_preprocessed2.json', 'w') as outfile:
            json.dump(data, outfile)
            print(data)

except Exception as e:
    print(e)
    print("Error!")
print("###################### Successfully! Created JSON file. ##############################")

