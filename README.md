### InfoVis - Visualization Guidelines for Big Data Representation

This project aims to exemplify visualizations that make Big Data visible, from the transformation of Big Data to Small Data. Techniques found in the literature for this purpose are used.

#### Prerequisites to use scrit Python

1. Install the necessary packages for the project (Python 3) - `pip3 install -r requirements.txt`

##### Analyze data using Jupyter Notebook or Python script

**Script Python** (Similar to Jupyter Notebook) - Running only in Ubuntu. In Windows, it is necessary to use _Jupyter Notebook_ or _Spyder_.

1. First, you need to download the ENEM 2019 dataset used in the project. To do this, access the [Kaggle page](https://www.kaggle.com/saraivaufc/enem-2019) and download the file from **DADOS > MICRODADOS_ENEM_2019.csv**.

2. Enter the _main.py_ file and change the _path_ where the ENEM dataset was placed in the **filename variable**.

3. Just run the script Python with the following command - `python3 main.py`

**Jupyter Notebook**

1. If you do not have the Jupyter Notebook application installed on the machine, just install the open-source distribution of Python and R languages called **Anaconda** (ideal for large-scale data processing) - [Installing on Windows](https://docs.anaconda.com/anaconda/install/windows/).

2. To enter the Jupyter Notebook platform, just follow the steps in this [tutorial](https://docs.anaconda.com/ae-notebooks/4.2.2/user-guide/basic-tasks/apps/jupyter/?highlight=using%20jupyter).

3. And to open the project's Jupyter notebook, just go to the repository path through the platform (you must have cloned the repository before), and enter the Notebook folder.

##### How to execute the D3 visualization:

**Steps:**

1. Clone the repository - `git clone https://gitlab.com/infovis-uerj/infovis-big-data.git`

2. Navigate to the folder with the project's source code related to D3 - `cd infovis-big-data/d3-visualization`

3. Install the HTTP server to allows reading local files in modern browsers - `npm install --global http-server`

4. Execute the following command - `http-server`

5. Access one of the pages available in the terminal in a web browser.
