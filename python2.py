import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

# Support material: https://towardsdatascience.com/pca-using-python-scikit-learn-e653f8989e60

# Change this path
filename = r"C:\Users\Suelen\Downloads\MICRODADOS_ENEM_2019.csv"

pd.options.display.max_columns = None
df = pd.read_csv(filename, sep=";", encoding="ISO-8859-1", chunksize=1000000)
df = pd.concat(df, ignore_index=True)
df.head()

# Applying SEGMENTATION technique called Sampling
# column = axis 1
# row = axis 0
# sample_df = df.sample(n=4, axis=1)
sample_df = df.sample(n=1000, axis=0)

# Creates new column, according to the region, to insert in sample_df
# Nordeste     - Northeast
# Norte        - North
# Centro-Oeste - Midwest
# Sudeste      - Southeast
# Sul          - South

regions = {"AL": "Northeast", "PE": "Northeast", "PB": "Northeast", "SE": "Northeast", "BA": "Northeast", "RN": "Northeast", "PI": "Northeast", "MA": "Northeast", "CE": "Northeast", 
           "AC": "North", "AM": "North", "AP": "North", "PA": "North", "TO": "North", "RR": "North", "RO": "North",
           "DF": "Midwest", "MT": "Midwest", "MS": "Midwest", "GO": "Midwest",
           "RJ": "Southeast", "SP": "Southeast", "ES": "Southeast", "MG": "Southeast", 
           "PR": "South", "RS": "South", "SC": "South"}

sample_df.insert(0, "REGIONS", df["SG_UF_PROVA"].map(regions), True)

sample_df = sample_df.dropna() # just in case (see if it is necessary)
sample_df = sample_df.reset_index(drop=True)
X = sample_df.drop(["REGIONS"], axis=1)

X = pd.get_dummies(X, prefix_sep='_')
X = StandardScaler().fit_transform(X) 

# Applying DATA REDUCTION technique called PCA
pca = PCA(n_components=2)
X_pca = pca.fit_transform(X)
PCA_df = pd.DataFrame(data=X_pca, columns=["PC1", "PC2"])
PCA_df = pd.concat([PCA_df, sample_df['REGIONS']], axis=1)

# Plot graph
fig = plt.figure(figsize = (8,8))
ax = fig.add_subplot(1,1,1) 
ax.set_xlabel('Principal Component 1', fontsize = 15)
ax.set_ylabel('Principal Component 2', fontsize = 15)
ax.set_title('2D PCA', fontsize = 20)
targets = ["North", "Northeast", "Midwest", "South", "Southeast"]
colors = ['r', 'g', 'b', 'c', 'y']

for target, color in zip(targets, colors):
    indicesToKeep = PCA_df["REGIONS"] == target
    ax.scatter(PCA_df.loc[indicesToKeep, 'PC1'],
               PCA_df.loc[indicesToKeep, 'PC2'],
               c = color,
               s = 50)
ax.legend(targets)
ax.grid()
