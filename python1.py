import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA, IncrementalPCA

# Change this path
filename = "C:/Users/rycba/OneDrive/Documentos/dados_enem/MICRODADOS_ENEM_2019.csv"

pd.options.display.max_columns = None
df = pd.read_csv(filename, sep=";", encoding="ISO-8859-1", chunksize=1000000)
df = pd.concat(df, ignore_index=True)
df.head()


#df = df[['SG_UF_PROVA', 'NU_ANO', 'CO_UF_RESIDENCIA', 'NU_IDADE', 'TP_ESTADO_CIVIL', 'TP_COR_RACA', 'TP_NACIONALIDADE', 'CO_UF_NASCIMENTO', 'TP_ST_CONCLUSAO', 'TP_ANO_CONCLUIU', 'TP_ESCOLA', 'TP_ENSINO', 'IN_TREINEIRO', 'CO_ESCOLA', 'CO_UF_ESC', 'TP_DEPENDENCIA_ADM_ESC', 'TP_LOCALIZACAO_ESC', 'TP_SIT_FUNC_ESC', 'IN_BAIXA_VISAO', 'IN_CEGUEIRA', 'IN_SURDEZ', 'IN_DEFICIENCIA_AUDITIVA', 'IN_SURDO_CEGUEIRA', 'IN_DEFICIENCIA_FISICA', 'IN_DEFICIENCIA_MENTAL', 'IN_DEFICIT_ATENCAO', 'IN_DISLEXIA', 'IN_DISCALCULIA', 'IN_AUTISMO', 'IN_VISAO_MONOCULAR', 'IN_OUTRA_DEF', 'IN_GESTANTE', 'IN_LACTANTE', 'IN_IDOSO', 'IN_ESTUDA_CLASSE_HOSPITALAR', 'IN_SEM_RECURSO', 'IN_BRAILLE', 'IN_AMPLIADA_24', 'IN_AMPLIADA_18', 'IN_LEDOR', 'IN_ACESSO', 'IN_TRANSCRICAO', 'IN_LIBRAS', 'IN_TEMPO_ADICIONAL', 'IN_LEITURA_LABIAL', 'IN_MESA_CADEIRA_RODAS', 'IN_MESA_CADEIRA_SEPARADA', 'IN_APOIO_PERNA', 'IN_GUIA_INTERPRETE', 'IN_COMPUTADOR', 'IN_CADEIRA_ESPECIAL', 'IN_CADEIRA_CANHOTO', 'IN_CADEIRA_ACOLCHOADA', 'IN_PROVA_DEITADO', 'IN_MOBILIARIO_OBESO', 'IN_LAMINA_OVERLAY', 'IN_PROTETOR_AURICULAR', 'IN_MEDIDOR_GLICOSE', 'IN_MAQUINA_BRAILE', 'IN_SOROBAN', 'IN_MARCA_PASSO', 'IN_SONDA', 'IN_MEDICAMENTOS', 'IN_SALA_INDIVIDUAL', 'IN_SALA_ESPECIAL', 'IN_SALA_ACOMPANHANTE', 'IN_MOBILIARIO_ESPECIFICO', 'IN_MATERIAL_ESPECIFICO', 'IN_NOME_SOCIAL', 'CO_UF_PROVA', 'TP_PRESENCA_CN', 'TP_PRESENCA_CH', 'TP_PRESENCA_LC', 'TP_PRESENCA_MT', 'CO_PROVA_CN', 'CO_PROVA_CH', 'CO_PROVA_LC', 'CO_PROVA_MT', 'NU_NOTA_CN', 'NU_NOTA_CH', 'NU_NOTA_LC', 'NU_NOTA_MT', 'TP_LINGUA', 'TP_STATUS_REDACAO', 'NU_NOTA_COMP1', 'NU_NOTA_COMP2', 'NU_NOTA_COMP3', 'NU_NOTA_COMP4', 'NU_NOTA_COMP5', 'NU_NOTA_REDACAO']]
df = df[['SG_UF_PROVA', 'NU_IDADE', 'TP_COR_RACA', 'TP_NACIONALIDADE']]

df = df.dropna()
y = df['SG_UF_PROVA']
df = df.drop(columns=['SG_UF_PROVA'])
print(df['NU_IDADE'].value_counts())


target_names = ["AL", "PE", "PB", "SE", "BA", "RN", "PI", "MA", "CE", "AC", "AM", "AP", "PA", "TO", "RR", "RO", "DF", "MT", "MS", "GO","RJ", "SP", "ES", "MG", "PR", "RS", "SC"]

colors = ['#36E3E7','#5E7995','#DF6D67','#48A15B','#ED1452','#B4DC4A','#74398A','#68F1E7','#CFD2B4','#9DD4BD','#FC9911','#1D0EB8','#0F6392','#54D0B6','#271894','#C8E6E1','#7C9AFA','#4E6FAE','#7C024C','#0DCF14','#66DFAF','#75559B','#F1E3B5','#451E1A','#BAE340','#2AC0C1']

xd = zip(colors, target_names, target_names)

n_components = 2
ipca = IncrementalPCA(n_components=n_components, batch_size=10)
X_ipca = ipca.fit_transform(df)

pca = PCA(n_components=n_components)
X_pca = pca.fit_transform(df)

#file = open("AAA.txt", "w")


#print(X_pca.size)
#print(X_ipca.size)
#for i in range (0, X_pca.size-1):
#    file.write(str(X_pca[i]) + "\n")


#   file.close()
for X_transformed, title in [(X_ipca, "Incremental PCA"), (X_pca, "PCA")]:
    plt.figure(figsize=(8, 8))
    for color, i, target_name in xd:

        plt.scatter(X_transformed[y == i, 0], X_transformed[y == i, 1],
                    color=color, lw=2, label=target_name)

    if "Incremental" in title:
        err = np.abs(np.abs(X_pca) - np.abs(X_ipca)).mean()
        plt.title(title + " of enem dataset\nMean absolute unsigned error "
                  "%.6f" % err)
    else:
        plt.title(title + " of enem dataset")
    plt.legend(loc="best", shadow=False, scatterpoints=1)
    plt.axis([-30.0, 80.0, -3.0, 4.0])

plt.show()