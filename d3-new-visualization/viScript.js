var width = 960,
    height = 500,
    centered;

var path = d3.geoPath()
var zoom = d3.zoom().scaleExtent([0.3,6]).on("zoom", zoomed);
var active = d3.select(null);

var atualZoom;

var svg = d3.select("body").append("svg")
  .attr("width", width)
  .attr("height", height)
  //.on("click", stopped, true);

svg.append("rect")
  .attr("class", "background")
  .attr("width", width)
  .attr("height", height)
  .on("click", reset);

svg.call(zoom);

var g = svg.append("g");

var data, 
    states = {};

d3.json("maps/topoJsonMaps/br_muns.json", function(error, br) {
  if (error) throw error;
  data = br;

  var featureCollection = topojson.feature(br, br.objects.br_muns);
  var bounds = d3.geoBounds(featureCollection);
  
  var centerX = d3.sum(bounds, function(d) {return d[0];}) / 2,
      centerY = d3.sum(bounds, function(d) {return d[1];}) / 2;

  var projection = d3.geoMercator()
    .scale(650)
    .center([centerX, centerY]);
    
  path.projection(projection);

  //getOnlyStates();
  drawMap(featureCollection);
});


function getStatesBorders() {
  var geometries = data.objects.br_muns.geometries.reduce(function (agrup, element) {
    if (!agrup[element.properties.uf]) {
      agrup[element.properties.uf] = {};
      agrup[element.properties.uf].arcs = [];
      agrup[element.properties.uf].properties = {}
      agrup[element.properties.uf].properties.populacao = 0;
    }
    agrup[element.properties.uf].arcs = agrup[element.properties.uf].arcs.concat(element.arcs[0]);
    agrup[element.properties.uf].properties.populacao = agrup[element.properties.uf].properties.populacao + parseInt(element.properties.populacao)
    return agrup
  }, {});
  states.type = "GeometryCollection"
}


function drawMap(featureCollection) {
  g.append("g")
    .selectAll("path")
    .data(featureCollection.features)
    .enter().append("path")
    .attr("class", function (d){
      return "feature" + " " + d.properties.uf
    })
    .attr("d", path)
    .on("click", clicked);
  
  g.append("path")
    .datum(topojson.mesh(data, data.objects.br_muns, function(a, b) {
      if (a.properties.nome == "Pedro Canário" || b.properties.nome == "Pedro Canário") {
        var aqui = "ES";
      }
      return a.properties.estado_id != b.properties.estado_id && a !== b;
    }))
    .attr("class", "mesh")
    .attr("d", path);
  
}

function clicked(d) {
  if (active.node() === this) return reset();
  active.classed("active", false);
  active = d3.select(this).classed("active", true);

  var bounds = path.bounds(d),
      dx = bounds[1][0] - bounds[0][0],
      dy = bounds[1][1] - bounds[0][1],
      x = (bounds[0][0] + bounds[1][0]) / 2,
      y = (bounds[0][1] + bounds[1][1]) / 2,
      scale = Math.max(1, Math.min(8, 0.9 / Math.max(dx / width, dy / height))),
      translate = [width / 2 - scale * x, height / 2 - scale * y];

  svg.transition()
      .duration(750)
      // .call(zoom.translate(translate).scale(scale).event); // not in d3 v4
      .call( zoom.transform, d3.zoomIdentity.translate(translate[0],translate[1]).scale(scale) ); // updated for d3 v4
}

function reset() {
  active.classed("active", false);
  active = d3.select(null);

  svg.transition()
      .duration(750)
      // .call( zoom.transform, d3.zoomIdentity.translate(0, 0).scale(1) ); // not in d3 v4
      .call( zoom.transform, d3.zoomIdentity ); // updated for d3 v4
}

function zoomed() {
  console.log(d3.event.transform)
  g.style("stroke-width", 1.5 / d3.event.transform.k + "px");
  // g.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")"); // not in d3 v4
  g.attr("transform", d3.event.transform); // updated for d3 v4
}

// If the drag behavior prevents the default click,
// also stop propagation so we don’t click-to-zoom.
function stopped() {
  if (d3.event.defaultPrevented) d3.event.stopPropagation();
}