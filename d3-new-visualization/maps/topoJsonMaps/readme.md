###To convert the geoJson to topoJson, must follow the steps:

    1. install `npm install -g topojson`
    2. run `geo2topo <file_name> > <output_name>.json`
    
More details in https://github.com/topojson/topojson and https://medium.com/@mbostock/command-line-cartography-part-3-1158e4c55a1e
