var margin = {top: 55, right: 20, bottom: 40, left: 100},
  width = 960 - margin.left - margin.right,
  height = 450 - margin.top - margin.bottom;

var barColorBrazil = "SteelBlue";
var barColorRegion = "SeaGreen";

var label = {
  title: "Histogram of the student averages who took the ENEM 2019",
  x: "Average grades",
  y: "Number of students"
}

//var numOfBins = 0;
//var stepSize = 0;
var scale = "logarithmic"
var dataJson = {};
var svgBrazil, svgRegion;
var legend;

var xScale = d3.scale.linear()
  .range([0, width])
  .domain([0, 1000]);

var yScale = d3.scale.log()
  .range([height, 0]);

var xAxis = d3.svg.axis()
  .scale(xScale)
  .orient("bottom").ticks(10);

var yAxis = d3.svg.axis()
  .scale(yScale)
  .orient("left")
  .tickFormat(function (d) {return yScale.tickFormat(4,d3.format(",d"))(d)})
  .ticks(10);

var svg = d3.select('#histogram').append('svg')
  .attr('width', width + margin.right + margin.left)
  .attr('height', height + margin.top + margin.bottom + 10)

d3.select("#info")
  .on("mouseover", function() {
    d3.select("#more-info")
      .classed("desactive", false);
  })
  .on("mouseout", function() {
    d3.select("#more-info")
      .classed("desactive", true);
  })

d3.select("#swap-brazil")
  .on("click", function() {
    svgBrazil.classed("hide", !svgBrazil.classed("hide"));
    legend.selectAll(".legend-brazil").classed("hide", svgBrazil.classed("hide"));
  })

function changeScale(scale, hist) {
  if (scale == "logarithmic") {
    yScale = d3.scale.log()
      .range([height, 0])
      .domain([1e-1, d3.max(hist, function(d) {return d.y})]);

    yAxis = d3.svg.axis()
      .scale(yScale)
      .orient("left")
      .tickFormat(function (d) {return yScale.tickFormat(4,d3.format(",d"))(d)})
      .ticks(10);

  } if (scale == "linear") {
    yScale = d3.scale.linear()
      .range([height, 0])
      .domain([0, d3.max(hist, function(d) { return d.y; })]);

    yAxis = d3.svg.axis()
      .scale(yScale)
      .orient("left")
      .ticks(10);
  }
  d3.select(".y.axis").call(yAxis)
}

function getColorScale(hist, region){
  var yMax = d3.max(hist, function(d){return d.y});
  var yMin = d3.min(hist, function(d){return d.y});
  if (region != "Brazil") {
    return d3.scale.linear()
      .domain([yMin, yMax])
      .range([d3.rgb(barColorRegion).brighter(), d3.rgb(barColorRegion).darker()]);
  }
  return d3.scale.linear()
    .domain([yMin, yMax])
    .range([d3.rgb(barColorBrazil).brighter(), d3.rgb(barColorBrazil).darker()]);
}

function addTooltip(data, mouseCoord) {
  var tooltip = moveTooltip(mouseCoord);
  tooltip.select(".count")
    .html("Count students: " + data.y);
  tooltip.select(".interval")
    .html("\
      Average interval: "
      + (data.x == xScale.domain()[0] ? data.x : Math.floor(data.x))
      + " - " 
      + (((data.x + stepSize) >= xScale.domain()[1]) ? xScale.domain()[1] : (Math.floor(stepSize) + Math.floor(data.x)))
    )
}

function moveTooltip(mouseCoord) {
  return d3.select("#tooltip-histogram")
    .style("left", (mouseCoord[0] + 50) + "px")
    .style("top", (mouseCoord[1]) + "px")
    .classed("desactive", false);
}

function closeTooltip() {
  d3.select("#tooltip-histogram")
    .classed("desactive", true)
}

function handleMouseOver(data) {
  addTooltip(data, d3.mouse(this))
}

function handleMouseMove() {
  moveTooltip(d3.mouse(this))
}

function handleMouseOut(data) {
  closeTooltip()
}

function addLabelsAxis() {
  svg.append("text")
    .attr("class", "x label")
    .attr("text-anchor", "middle")
    .attr("x", width/2 + margin.left)
    .attr("y", height + margin.top + margin.bottom)
    .text(label.x);

  svg.append("text")
    .attr("class", "title label")
    .attr("text-anchor", "middle")
    .attr("x", width/2 + margin.left)
    .attr("y", margin.top - 35)
    .text(label.title);

  svg.append("text")
    .attr("class", "y label")
    .attr("text-anchor", "middle")
    .attr("y", 2 * margin.right - 8)
    .attr("x", - (height + margin.top + margin.bottom)/2)
    .attr("transform", "rotate(-90)")
    .text(label.y);

  svg = svg.append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

  // draw the x axis
  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

  // draw the x axis
  svg.append("g")
    .attr("class", "y axis")
    .call(yAxis);
}

function processData(dataAmount, stepSize) {
  proData = []
  dataAmount.map(function(bin, index) {
    proData.push({
      "y": bin,
      "x": index * stepSize
    })
  })
  return proData;
}

function addBrazil() {
  if (dataJson["Brazil"] == null) {
    for (region in dataJson) {
      if (dataJson["Brazil"] == null) {
        dataJson["Brazil"] = Object.create(dataJson[region])
      } else {
        for (bin in dataJson[region]) {
          dataJson["Brazil"][bin] = dataJson[region][bin].map(function(amount, index){
            return amount + dataJson["Brazil"][bin][index]
          })
        }
      }
    }
  }
}

function selectRegion(zone) {
  return dataJson[zone]
}

function addRegionToForm() {
  var select = d3.select("#region");
  for (region in dataJson) {
    select.append("option")
      .attr("value", region)
      .html(region);
  }
  d3.select("#region").node().value = "Brazil"
}

function makeSvgGroups() {
  svgBrazil = svg.append("g").attr("class", "brazil");
  svgRegion = svg.append("g").attr("class", "region");
}

function addLegends(num, region) {
  //var barColorBrazil = "SteelBlue";
  //var barColorRegion = "SeaGreen";
  if (legend) {
    legend.remove();
  }
  legend = svg.append("g").attr("class", "legend");

  if (num == 1) {
    var color;
    if (region == "Brazil") {
      color = barColorBrazil
    } else {
      color = barColorRegion
    }

    legend.append("rect")
      .attr("height", "15px")
      .attr("width", "20px")
      .attr("x", (width - margin.right) - 20)
      .attr("y", 0)
      .attr("fill", color)
      .style("stroke", d3.rgb(color).darker())
      .style("stroke-width", "2px");

    legend.append("text")
      .attr("x", (width - margin.right) - 20 + 25)
      .attr("y", 15)
      .text(region)
  } else {
    legend.append("rect")
      .attr("height", "15px")
      .attr("width", "20px")
      .attr("x", (width - margin.right) - 30)
      .attr("y", 0)
      .attr("fill", barColorRegion)
      .style("stroke", d3.rgb(barColorRegion).darker())
      .style("stroke-width", "2px");

    legend.append("text")
      .attr("x", (width - margin.right) - 20 + 15)
      .attr("y", 15)
      .text(region)

    legend.append("rect")
      .attr("height", "15px")
      .attr("width", "20px")
      .attr("class", "legend-brazil")
      .attr("x", (width - margin.right) - 30)
      .attr("y", 20)
      .attr("fill", barColorBrazil)
      .style("stroke", d3.rgb(barColorBrazil).darker())
      .style("stroke-width", "2px");
      
    legend.append("text")
      .attr("x", (width - margin.right) - 20 + 15)
      .attr("y", 35)
      .text("Brazil")
      .attr("class", "legend-brazil");
  }
}

// Get the data
d3.json("ENEM_preprocessed.json", function(error, dataPrePro) {
  dataJson = dataPrePro;
  addBrazil();
  addRegionToForm();
  addLabelsAxis();
  addLegends(1, "Brazil");
  makeSvgGroups();
  update();
});

d3.selectAll("#numOfBins").on("change", update);
d3.selectAll("#scale").on("change", update);
d3.selectAll("#region").on("change", update);

function update() {
  var region = d3.select("#region").node().value;
  numOfBins = parseInt(d3.select("#numOfBins").node().value);
  scale = d3.select("#scale").node().value;
  stepSize = Math.abs(xScale.domain()[1] - xScale.domain()[0]) / numOfBins;
  
  if (region == "Brazil") {
    var data = selectRegion("Brazil");
    var hist = processData(data[numOfBins], stepSize);
    changeScale(scale, hist);
    updateHistogram(svgBrazil, "Brazil", hist, stepSize);
    svgBrazil.classed("hide", false);
    svgRegion.classed("hide", true);
    d3.select("#swap-brazil").classed("hide", true);
    svgBrazil.selectAll(".text").classed("hide", false);
    addLegends(1, "Brazil");
  } else {
    var data = selectRegion("Brazil");
    var hist = processData(data[numOfBins], stepSize);
    changeScale(scale, hist);
    updateHistogram(svgBrazil, "Brazil", hist, stepSize);

    data = selectRegion(region);
    hist = processData(data[numOfBins], stepSize);
    updateHistogram(svgRegion, region, hist, stepSize);
    svgRegion.classed("hide", false);
    svgRegion.selectAll(".text").classed("hide", svgBrazil.classed("hide"));
    d3.select("#swap-brazil").classed("hide", false);

    if (scale == "linear") {
      svgBrazil.selectAll(".text").classed("hide", true);
      svgRegion.selectAll(".text").classed("hide", false);
    } else {
      svgBrazil.selectAll(".text").classed("hide", false);
      svgRegion.selectAll(".text").classed("hide", false);
    }
    addLegends(2, region);
  }
}

function updateHistogram(svg, region, hist, stepSize) {
  colorScale = getColorScale(hist, region);
  xBinwidth = (width / hist.length) - 1;

  svg.selectAll(".bar").data(hist)
    .enter().append("rect")
    .attr("class", "bar");

  svg.selectAll(".bar").data(hist)
    .attr("height", function(d) {
      if (isNaN(yScale(d.y))) {
        return yScale(yScale.domain()[0]) * 0.005
      }
      if (((height - yScale(d.y)) / (height - yScale(d3.max(hist, function(d){return d.y})))) < 0.0025) {
        return height - (yScale(d3.max(hist, function(d){return d.y}) * 0.0025))
      }
      return height - yScale(d.y)
    })
    .attr("width", function(d) {return xBinwidth})
    .attr("fill", function(d) {return colorScale(d.y)})
    .attr("x", function(d) {return xScale(d.x)})
    .attr("y", function(d) {
      if (isNaN(yScale(d.y))) {
        return height - yScale(yScale.domain()[0]) * 0.005
      }
      if (((height - yScale(d.y)) / (height - yScale(d3.max(hist, function(d){return d.y})))) < 0.0025) {
        return (yScale(d3.max(hist, function(d){return d.y}) * 0.0025))
      }
      return yScale(d.y)
    })
    .on("mouseover", handleMouseOver)
    .on("mouseout", handleMouseOut)
    .on("mousemove", handleMouseMove);

  svg.selectAll(".bar").data(hist).exit()
      .remove();

  if ((stepSize >= Math.abs(xScale.domain()[1] - xScale.domain()[0]) * 0.05)) {
    svg.selectAll(".text").data(hist)
      .enter().append("text")
      .attr("class", "text")
      .attr("text-anchor", "middle");

    svg.selectAll(".text").data(hist)
      .attr("x", function(d) {return xScale(d.x + (stepSize / 2))})
      .attr("y", function(d) {
        if (isNaN(yScale(d.y))) {
          return height - 4
        }
        if ((height - yScale(d.y)) < 15) {
          return yScale(d.y) - 4;
        }
        return yScale(d.y) + 14;
      })
      .text(function(d) { return d.y; })
      .classed("above", function(d) {
        return isNaN(yScale(d.y)) || (height - yScale(d.y)) < 15;
      })
      .on("mouseover", handleMouseOver)
      .on("mouseout", handleMouseOut)
      .on("mousemove", handleMouseMove);

    svg.selectAll(".text").data(hist).exit()
        .remove();
  } else {
    svg.selectAll(".text").data(hist).remove();
  }
}