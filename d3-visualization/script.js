var margin = {top: 30, right: 20, bottom: 30, left: 50},
  width = 960 - margin.left - margin.right,
  height = 450 - margin.top - margin.bottom;

var barColor = "steelblue";
var label = {
  x: "Média das Notas"
}

var xScale = d3.scale.linear()
  .range([0, width])
  .domain([1000, 3000]);;

var yScale = d3.scale.linear()
  .range([height, 0]);

var xAxis = d3.svg.axis()
  .scale(xScale)
  .orient("bottom").ticks(5);

var yAxis = d3.svg.axis()
  .scale(yScale)
  .orient("left").ticks(5);

var svg = d3.select('body').append('svg')
  .attr('width', width + margin.right + margin.left)
  .attr('height', height + margin.top + margin.bottom)
  .append('g')
  .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

function getColorScale(hist){
  var yMax = d3.max(hist, function(d){return d.length});
  var yMin = d3.min(hist, function(d){return d.length});
  return d3.scale.linear()
    .domain([yMin, yMax])
    .range([d3.rgb(barColor).brighter(), d3.rgb(barColor).darker()]);
}

function addTooltip(data, mouseCoord) {
  d3.select(".tooltip")
    .style("left", mouseCoord[0] + 50 + "px")
    .style("top", mouseCoord[1] + "px")
    .classed("desactive", false)
    .select("span")
    .html(data.y);
}

function moveTooltip(mouseCoord) {
  d3.select(".tooltip")
    .style("left", mouseCoord[0] + 50 + "px")
    .style("top", mouseCoord[1] + "px")
}

function closeTooltip() {
  d3.select(".tooltip")
    .classed("desactive", true)
}

function handleMouseOver(data) {
  addTooltip(data, d3.mouse(this))
}

function handleMouseMove() {
  moveTooltip(d3.mouse(this))
}

function handleMouseOut(data) {
  closeTooltip()
}

// Get the data
d3.tsv("data.tsv", function(error, data) {
  data.forEach(function(d) {
    d.x = parseFloat(d.x); // id
    d.y = parseInt(d.y);
  });

  var binwidth = parseInt(d3.select("#binwidth").node().value);

  hist = d3.layout.histogram()
    .bins(d3.range(xScale.domain()[0], xScale.domain()[1]+binwidth, binwidth)) 
    (data.map(function(d) {return d.x; }));

  colorScale = getColorScale(hist);

  xBinwidth = width / hist.length -1;
  yScale.domain([0, d3.max(hist, function(d) { return d.y; })])

  svg.selectAll(".bar")
    .data(hist)
    .enter().append("rect")
    .attr("class", "bar")
    .attr("width", function(d) { return xBinwidth })
    .attr("height", function(d) { return height- yScale(d.y)-1; })
    .attr("fill", function(d) {return colorScale(d.y)})
    .attr("x", function(d) {return xScale(d.x)})
    .attr("y", function(d) {return yScale(d.y)})
    .on("mouseover", handleMouseOver)
    .on("mouseout", handleMouseOut)
    .on("mousemove", handleMouseMove);

  svg.selectAll(".text")
    .data(hist)
    .enter()
    .append("text")
    .attr("class", "text")
    .attr("x", function(d) {return xScale(d.x + binwidth/2)})
    .attr("y", function(d) {return yScale(d.y) + 14})
    .attr("text-anchor", "middle")
    .text(function(d) { return d.y; });

  svg.append("text")
    .attr("class", "x label")
    .attr("text-anchor", "middle")
    .attr("x", width/2)
    .attr("y", height + margin.bottom-3)
    .text(label.x);

  d3.selectAll("#binwidth").on("change", update);

  // draw the x axis
  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

  // draw the x axis
  svg.append("g")
    .attr("class", "y axis")
    .call(yAxis);


  function update() {
    binwidth = parseInt(d3.select("#binwidth").node().value);

    hist = d3.layout.histogram()
      .bins(d3.range(xScale.domain()[0], xScale.domain()[1]+binwidth, binwidth))
      (data.map(function(d) {return d.x; }));

    colorScale = getColorScale(hist);

    yScale.domain([0, d3.max(hist, function(d) { return d.y; })]);
    xBinwidth =  width / hist.length -1

    svg.selectAll(".bar").data(hist)
      .enter().append("rect")
      .attr("class", "bar");

    svg.selectAll(".bar").data(hist)
      .attr("height", function(d) { return height- yScale(d.y)-1; })
      .attr("width", function(d) { return xBinwidth; })
      .attr("fill", function(d) {return colorScale(d.y)})
      .attr("x", function(d) {return xScale(d.x)})
      .attr("y", function(d) {return yScale(d.y)})
      .on("mouseover", handleMouseOver)
      .on("mouseout", handleMouseOut)
      .on("mousemove", handleMouseMove);

    svg.selectAll(".bar").data(hist).exit()
       .remove();

    if (binwidth > 20) {

      svg.selectAll(".text").data(hist)
        .enter().append("text")
        .attr("class", "text")
        .attr("text-anchor", "middle");

      svg.selectAll(".text").data(hist)
        .attr("x", function(d) {return xScale(d.x + binwidth/2)})
        .attr("y", function(d) {return yScale(d.y) + 14 })
        .text(function(d) { return d.y; })

      svg.selectAll(".text").data(hist).exit()
         .remove();
    } else {
      svg.selectAll(".text").data(hist).remove();
    }

    d3.select(".y.axis").call(yAxis);
  }
});